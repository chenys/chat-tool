import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { NavBar } from 'antd-mobile';
import NavLinkBar from './component/navLinkBar';
import { getMsgList, sendMsg, recvMsg } from './redux/action';
import './admin.css';

@connect(
    state => state,
    { getMsgList, sendMsg, recvMsg }
)
@withRouter
class Admin extends React.Component {
    componentDidMount() {
        if (!this.props.chat.msgList.length) {
            this.props.getMsgList();
            this.props.recvMsg();
        }
    }

    render() {
        const { user: { identity }, location: { pathname } } = this.props;
        const navList = [
            {
                path: '/boss',
                text: '牛人',
                icon: 'boss',
                title: '牛人列表',
                hide: identity === 'genius'
            },
            {
                path: '/genius',
                text: 'boss',
                icon: 'job',
                title: 'BOSS列表',
                hide: identity === 'boss'
            },
            {
                path: '/message',
                text: '消息',
                icon: 'message',
                title: '消息列表',
            },
            {
                path: '/me',
                text: '我',
                icon: 'user',
                title: '个人中心',
            }
        ];
        const currentNav = navList.find(v => v.path === pathname);
        return (
            <div>
                <div className="fixed-header">
                    <NavBar mode='dark'>
                        {currentNav ? currentNav.title : ""}
                    </NavBar>
                </div>
                <div style={{ marginTop: 45 }}>
                    {this.props.children}
                </div>
                <div className="fixed-footer">
                    <NavLinkBar data={navList} />
                </div>
            </div>
        );
    }
}

export default Admin;