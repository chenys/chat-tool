export function getRedirectPath({ identity, avatar }) {
    // 根据用户信息，返回跳转地址
    let url = (identity === 'boss') ? '/boss' : '/genius';
    if (!avatar) {
        url += 'info';
    }
    return url;
}

export function getChatId(userId, targetId) {
    return [userId, targetId].sort().join('_');
}