import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { auth } from '../../redux/action';

@withRouter
@connect(
    null,
    { auth }
)
class AuthRoute extends React.Component {
    componentDidMount() {
        const publicList = ['/login', '/register'];
        const pathname = this.props.location.pathname;

        if (publicList.some(item => item === pathname)) {
            return null;
        }

        // 获取用户信息
        axios.get('/user/info')
            .then(res => {
                if (res.status === 200) {
                    if (res.data.code === 0) {
                        // 有登录信息
                        this.props.auth(res.data.data);
                    } else {
                        // 没有登录信息，那么就跳转到登录页面
                        this.props.history.push('/login');
                    }
                }
            })
        // 是否登录

        // 现在的url地址 login页面是不需要跳转的

        // 用户的type 身份是boss还是牛人

        // 用户是否完善信息（选择头像 个人简介）
    }

    render() {
        return null;
    }
}

export default AuthRoute;