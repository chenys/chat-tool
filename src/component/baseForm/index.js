import React from 'react';

export default function baseForm(Comp) {
    return class WrapperComp extends React.Component {
        constructor(props) {
            super(props);
            this.state = {};
            this.handleChange = this.handleChange.bind(this);
        }

        handleChange(key, value) {
            this.setState({ [key]: value });
        }

        render() {
            return <Comp
                handleChange={this.handleChange}
                state={this.state}
                {...this.props}     // 属性穿透：所有的属性直接传递给组件
            />
        }
    }
}