import React from 'react';
import LogoImg from '../../image/job.png';
import './index.css';

export default class Logo extends React.Component {
    render() {
        return (
            <div className="logo-container">
                <img src={LogoImg} alt="" />
            </div>
        );
    }
}