import React from 'react';
import { Card, WhiteSpace } from 'antd-mobile';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

@withRouter
class UserCard extends React.Component {
    static propTypes = {
        userlist: PropTypes.array.isRequired
    }

    render() {
        return (
            <div>
                {this.props.userlist.map((v) => (
                    v.avatar ? <div
                        key={v._id}
                        onClick={() => { this.props.history.push(`/chat/${v._id}/${v.user}`); }}
                    >
                        <Card>
                            <Card.Header
                                title={v.user}
                                thumb={require(`../../image/avatar/${v.avatar}`)}
                                extra={<span>{v.title}</span>}
                            />
                            <Card.Body>
                                {v.identity === 'boss' ? <div>公司：{v.company}</div> : null}
                                {v.desc.split('\n').map((d, id) => (
                                    <div key={id}>要求：{d}</div>
                                ))}
                                {v.identity === 'boss' ? <div>薪资：{v.salary}</div> : null}
                            </Card.Body>
                        </Card>
                        <WhiteSpace />
                    </div> : null
                ))}
            </div>
        );
    }
}

export default UserCard;