import React from 'react';
import { Grid, List } from 'antd-mobile';
import PropTypes from 'prop-types';

export default class AvatarSelector extends React.Component {
    static propTypes = {
        onClick: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const imgList = ["boy.png", "bull.png", "chick.png", "crab.png", "girl.png", "hedgehog.png", "hippopotamus.png", "koala.png", "lemur.png", "man.png", "pig.png", "tiger.png", "whale.png", "woman.png", "zebra.png"];
        const avatarList = imgList.map(v => ({
            icon: require(`../../image/avatar/${v}`),
            text: v
        }));
        const gridHeader = this.state.icon ? <div>
            <span>已选择头像</span>
            <img src={this.state.icon} alt="" width={15} />
        </div> : "请选择头像";

        return (
            <div>
                <List renderHeader={gridHeader}>
                    <Grid
                        data={avatarList}
                        columnNum={5}
                        onClick={elm => {
                            this.setState(elm);
                            this.props.onClick(elm.text);
                        }}
                    />
                </List>
            </div>
        );
    }
}