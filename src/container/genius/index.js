import React from 'react';
import axios from 'axios';
import UserCard from '../../component/usercard';

class Genius extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userlist: []
        }
    }

    componentDidMount() {
        axios.get('/user/list?identity=boss')
            .then(res => {
                if (res.data.code === 0) {
                    this.setState({ userlist: res.data.data });
                }
            })
    }

    render() {
        return <UserCard userlist={this.state.userlist} />;
    }
}

export default Genius;