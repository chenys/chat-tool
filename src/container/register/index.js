import React from 'react';
import Logo from '../../component/logo';
import baseForm from '../../component/baseForm';
import { List, InputItem, Radio, WingBlank, WhiteSpace, Button, Toast } from 'antd-mobile';
import { connect } from 'react-redux';
import { auth } from '../../redux/action';
import { Redirect } from 'react-router-dom'
import axios from 'axios';

@connect(
    state => state.user,
    { auth }
)
@baseForm
class Register extends React.Component {
    constructor(props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);
    }

    componentDidMount() {
        this.props.handleChange('identity', 'genius');
    }

    handleRegister() {
        const { user, pwd, repeatpwd, identity } = this.props.state;
        if (!user || !pwd) {
            Toast.info('用户名和密码必须输入');
            return;
        }
        if (pwd !== repeatpwd) {
            Toast.info('密码和确认密码必须相同');
            return;
        }
        axios.post('/user/register', { user, pwd, identity })
            .then(res => {
                if (res.status === 200 && res.data.code === 0) {
                    this.props.auth(res.data.data);
                    Toast.success('注册成功！');
                } else {
                    Toast.fail(res.data.msg);
                }
            })
    }

    render() {
        const { identity } = this.props.state;
        const RadioItem = Radio.RadioItem;
        return (
            <div>
                {this.props.redirectTo ? <Redirect to={this.props.redirectTo} /> : null}
                <Logo />
                <WingBlank>
                    <List>
                        <InputItem
                            onChange={v => this.props.handleChange('user', v)}
                        >用户名</InputItem>
                        <InputItem
                            type='password'
                            onChange={v => this.props.handleChange('pwd', v)}
                        >密码</InputItem>
                        <InputItem
                            type='password'
                            onChange={v => this.props.handleChange('repeatpwd', v)}
                        >确认密码</InputItem>
                    </List>
                    <WhiteSpace />
                    <RadioItem
                        checked={identity === 'genius'}
                        onClick={() => this.props.handleChange('identity', 'genius')}
                    >牛人</RadioItem>
                    <RadioItem
                        checked={identity === 'boss'}
                        onClick={() => this.props.handleChange('identity', 'boss')}
                    >BOSS</RadioItem>
                    <WhiteSpace />
                    <Button
                        type="primary"
                        onClick={this.handleRegister}
                    >注册</Button>
                </WingBlank>
            </div>
        );
    }
}

export default Register;