import React from 'react';
import axios from 'axios';
import UserCard from '../../component/usercard';

class Boss extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userlist: []
        }
    }

    componentDidMount() {
        this.getUserList();
    }

    async getUserList() {
        const res = await axios.get('/user/list?identity=genius')
        if (res.data.code === 0) {
            this.setState({ userlist: res.data.data });
        }
    }

    render() {
        return <UserCard userlist={this.state.userlist} />;
    }
}

export default Boss;