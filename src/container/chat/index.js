import React from 'react';
import { connect } from 'react-redux';
import { getMsgList, recvMsg, sendMsg, readMsg } from '../../redux/action';
import { List, InputItem, NavBar, Icon } from 'antd-mobile';
import { getChatId } from '../../utils';
import QueueAnim from 'rc-queue-anim';

@connect(
    state => state,
    { getMsgList, recvMsg, sendMsg, readMsg }
)

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
        }
    }

    componentDidMount() {
        if (!this.props.chat.msgList.length) {
            this.props.getMsgList();
            this.props.recvMsg();
        }
    }

    componentWillUnmount() {
        const to = this.props.match.params.userID;
        this.props.readMsg(to);
    }

    handleSubmit() {
        if (!this.state.text) {
            return;
        }
        const from = this.props.user._id;
        const to = this.props.match.params.userID;
        const content = this.state.text;
        this.props.sendMsg({ from, to, content });
        this.setState({ text: '' });
    }

    render() {
        const { user: target, userID: targetId } = this.props.match.params;
        const { chat: { msgList, users }, user } = this.props;

        // getChatId
        const chatid = getChatId(user._id, targetId);
        const chatMsgs = msgList.filter(v => v.chatid === chatid);
        return (
            <div id="chat-page">
                <NavBar
                    mode='dark'
                    icon={<Icon type="left" />}
                    onLeftClick={() => { this.props.history.goBack(); }}
                >{target}</NavBar>
                <QueueAnim delay={100}>
                    {chatMsgs.map((v, id) => {
                        const avatar = require(`../../image/avatar/${users[v.from].avatar}`);
                        return v.from === targetId ?
                            (
                                <List key={id}>
                                    <List.Item
                                        thumb={avatar}
                                    >{v.content}</List.Item>
                                </List>
                            ) :
                            (
                                <List key={id}>
                                    <List.Item
                                        className="chat-me"
                                        extra={<img src={avatar} alt="" />}
                                    >{v.content}</List.Item>
                                </List>
                            );
                    })}
                </QueueAnim>
                <div className="stick-footer">
                    <List>
                        <InputItem
                            placeholder="请输入"
                            value={this.state.text}
                            onChange={v => { this.setState({ text: v }) }}
                            extra={<span onClick={() => this.handleSubmit()}>发送</span>}
                        />
                    </List>
                </div>
            </div>
        );
    }
}

export default Chat;