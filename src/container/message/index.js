import React from 'react';
import { connect } from 'react-redux';
import { List, Badge } from 'antd-mobile';

const Item = List.Item;
const Brief = Item.Brief;

@connect(
    state => state
)
class Message extends React.Component {
    getLastItem(arr) {
        return arr[arr.length - 1];
    }

    render() {
        const { msgList, users } = this.props.chat;
        const msgGroup = {};
        msgList.forEach(v => {
            msgGroup[v.chatid] = msgGroup[v.chatid] || [];
            msgGroup[v.chatid].push(v);
        })
        // 按照时间戳降序排序
        const chatList = Object.values(msgGroup).sort((a, b) => {
            const a_last = this.getLastItem(a).create_time;
            const b_last = this.getLastItem(b).create_time;
            return b_last - a_last;
        });
        const userid = this.props.user._id;
        return (
            <div>
                <List>
                    {chatList.map((v, id) => {
                        const lastItem = this.getLastItem(v);
                        const targetId = (v[0].from === userid) ? v[0].to : v[0].from;
                        const unreadNum = v.filter(v => !v.read && v.to === userid).length;
                        return (
                            <Item
                                key={id}
                                extra={<Badge text={unreadNum} />}
                                thumb={require(`../../image/avatar/${users[targetId].avatar}`)}
                                arrow="horizontal"
                                onClick={() => {
                                    this.props.history.push(`/chat/${targetId}/${users[targetId].name}`);
                                }}
                            >
                                {lastItem.content}
                                <Brief>{users[targetId].name}</Brief>
                            </Item>
                        )
                    })}
                </List>
            </div>
        );
    }
}

export default Message;