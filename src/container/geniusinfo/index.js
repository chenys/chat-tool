import React from 'react';
import { NavBar, InputItem, TextareaItem, Button, WhiteSpace, Toast } from 'antd-mobile';
import { connect } from 'react-redux';
import AavtarSelector from '../../component/avatarSelector';
import { auth } from '../../redux/action';
import { Redirect } from 'react-router-dom'
import axios from 'axios';

@connect(
    state => state.user,
    { auth }
)
class GeniusInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            desc: "",
            avatar: "",
        }
    }

    handleChange(key, val) {
        this.setState({ [key]: val });
    }

    handleUpdate() {
        axios.post('/user/update', this.state)
            .then(res => {
                if (res.status === 200 && res.data.code === 0) {
                    this.props.auth(res.data.data);
                    Toast.success('保存成功！');
                } else {
                    Toast.fail(res.data.msg);
                }
            })
    }

    render() {
        return (
            <div>
                {this.props.redirectTo ? <Redirect to={this.props.redirectTo} /> : null}
                <NavBar mode="dark">牛人完善信息页</NavBar>
                <AavtarSelector
                    onClick={(avatar) => { this.setState({ avatar }); }}
                />
                <InputItem onChange={(v => this.handleChange('title', v))}>
                    求职岗位
                </InputItem>
                <TextareaItem
                    rows={3}
                    autoHeight
                    title="个人简介"
                    onChange={(v => this.handleChange('desc', v))}
                />
                <WhiteSpace />
                <Button onClick={() => this.handleUpdate()}>保存</Button>
            </div>
        );
    }
}

export default GeniusInfo;