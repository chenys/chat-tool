import React from 'react';
import Logo from '../../component/logo';
import baseForm from '../../component/baseForm';
import { List, InputItem, WingBlank, WhiteSpace, Button, Toast } from 'antd-mobile';
import { connect } from 'react-redux';
import { auth } from '../../redux/action';
import { Redirect } from 'react-router-dom'
import axios from 'axios';

@connect(
    state => state.user,
    { auth }
)
@baseForm
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.register = this.register.bind(this);
        this.login = this.login.bind(this);
    }

    register() {
        this.props.history.push('/register');
    }

    login() {
        const { user, pwd } = this.props.state;
        if (!user || !pwd) {
            Toast.info('用户名和密码必须输入');
            return;
        }
        axios.post('/user/login', { user, pwd })
            .then(res => {
                if (res.status === 200 && res.data.code === 0) {
                    this.props.auth(res.data.data);
                    Toast.success('登录成功！');
                } else {
                    Toast.fail(res.data.msg);
                }
            })
    }

    render() {
        return (
            <div>
                {this.props.redirectTo ? <Redirect to={this.props.redirectTo} /> : null}
                <Logo />
                <WingBlank>
                    <List>
                        <InputItem
                            onChange={v => this.props.handleChange('user', v)}
                        >用户名</InputItem>
                        <InputItem
                            type='password'
                            onChange={v => this.props.handleChange('pwd', v)}
                        >密码</InputItem>
                    </List>
                    <WhiteSpace />
                    <Button
                        type="primary"
                        onClick={this.login}
                    >登录</Button>
                    <WhiteSpace />
                    <Button
                        type="primary"
                        onClick={this.register}
                    >注册</Button>
                </WingBlank>
            </div>
        );
    }
}

export default Login;