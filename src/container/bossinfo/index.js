import React from 'react';
import { NavBar, InputItem, TextareaItem, Button, WhiteSpace, Toast } from 'antd-mobile';
import { connect } from 'react-redux';
import AavtarSelector from '../../component/avatarSelector';
import { auth } from '../../redux/action';
import { Redirect } from 'react-router-dom'
import axios from 'axios';

@connect(
    state => state.user,
    { auth }
)
class BossInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            company: "",
            salary: "",
            desc: "",
            avatar: "",
        }
    }

    handleChange(key, val) {
        this.setState({ [key]: val });
    }

    handleUpdate() {
        axios.post('/user/update', this.state)
            .then(res => {
                if (res.status === 200 && res.data.code === 0) {
                    this.props.auth(res.data.data);
                    Toast.success('保存成功！');
                } else {
                    Toast.fail(res.data.msg);
                }
            })
    }

    render() {
        return (
            <div>
                {this.props.redirectTo ? <Redirect to={this.props.redirectTo} /> : null}
                <NavBar mode="dark">BOSS完善信息页</NavBar>
                <AavtarSelector
                    onClick={(avatar) => { this.setState({ avatar }); }}
                />
                <InputItem onChange={(v => this.handleChange('title', v))}>
                    招聘职位
                </InputItem>
                <InputItem onChange={(v => this.handleChange('company', v))}>
                    公司名称
                </InputItem>
                <InputItem onChange={(v => this.handleChange('salary', v))}>
                    职位薪资
                </InputItem>
                <TextareaItem
                    rows={3}
                    autoHeight
                    title="职位要求"
                    onChange={(v => this.handleChange('desc', v))}
                />
                <WhiteSpace />
                <Button onClick={() => this.handleUpdate()}>保存</Button>
            </div>
        );
    }
}

export default BossInfo;