import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Result, List, WhiteSpace, Button, Modal } from 'antd-mobile';
import browserCookies from 'browser-cookies'
import { logout } from '../../redux/action';

@connect(
    state => state.user,
    { logout }
)
class Me extends React.Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout() {
        Modal.alert('注销', '确定退出登录吗？', [
            { text: '取消', onPress: () => { console.log('cancel') } },
            {
                text: '确定', onPress: () => {
                    browserCookies.erase('userid');
                    this.props.logout();
                }
            }
        ]);
    }

    render() {
        const { avatar, user, company, identity, title, desc, salary } = this.props;
        const Item = List.Item;
        const Brief = Item.Brief;
        return user ? (
            <div>
                <Result
                    img={<img src={require(`../../image/avatar/${avatar}`)} alt="" style={{ width: 40 }} />}
                    title={user}
                    message={identity === "boss" ? company : null}
                />
                <List renderHeader={() => '简介'}>
                    <Item
                        multipleLine
                    >
                        {title}
                        {desc.split('\n').map((v, id) => <Brief key={id}>{v}</Brief>)}
                        {salary ? <Brief>薪资：{salary}</Brief> : null}
                    </Item>
                </List>
                <WhiteSpace />
                <Button onClick={this.logout}>退出登录</Button>
            </div>
        ) : <Redirect to={this.props.redirectTo} />;
    }
}

export default Me;