import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AuthRoute from './component/authroute';
import Admin from './admin';
import Login from './container/login';
import Register from './container/register';
import BossInfo from './container/bossinfo';
import GeniusInfo from './container/geniusinfo';
import Boss from './container/boss';
import Genius from './container/genius';
import Message from './container/message';
import Me from './container/me';
import Chat from './container/chat';

export default class IRouter extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <AuthRoute />
                <Switch>
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                    <Route path="/bossinfo" component={BossInfo} />
                    <Route path="/geniusinfo" component={GeniusInfo} />
                    <Route path="/chat/:userID/:user" component={Chat} />
                    <Route path="/" render={() =>
                        <Admin>
                            <Switch>
                                <Route path="/boss" component={Boss} />
                                <Route path="/genius" component={Genius} />
                                <Route path="/message" component={Message} />
                                <Route path="/me" component={Me} />
                                <Route component={Login} />
                            </Switch>
                        </Admin>
                    } />
                </Switch>
            </BrowserRouter>
        );
    }
}