import axios from 'axios';
import { Toast } from 'antd-mobile';
import io from 'socket.io-client';

const socket = io('ws://localhost:9093');   // 连接socket

// Action 类型
export const type = {
    AUTH: 'AUTH',
    LOGOUT: 'LOGOUT',
    MSG_LIST: 'MSG_LIST',
    MSG_RECV: 'MSG_RECV',
    MSG_READ: 'MSG_READ',
};

// Action 函数
export function auth(data) {
    return { type: type.AUTH, payload: data };
}

export function logout() {
    return { type: type.LOGOUT };
}

function msgList(data, users, userid) {
    return { type: 'MSG_LIST', payload: { data, users, userid } }
}
export function getMsgList() {
    return (dispatch, getState) => {
        axios.get('/user/getMsgList')
            .then(res => {
                if (res.status === 200 && res.data.code === 0) {
                    const userid = getState().user._id;
                    dispatch(msgList(res.data.data, res.data.users, userid));
                } else {
                    Toast.fail(res.data.msg);
                }
            })
    }
}

export function sendMsg({ from, to, content }) {
    return dispatch => {
        socket.emit('sendmsg', { from, to, content });
    }
}

function msgRecv(data, userid) {
    return { type: 'MSG_RECV', payload: { data, userid } };
}
export function recvMsg() {
    return (dispatch, getState) => {
        // 监听来自socket的消息
        socket.on('recvmsg', function (data) {
            const userid = getState().user._id;
            dispatch(msgRecv(data, userid));
        })
    }
}

function msgRead({ from, userid, num }) {
    return { type: 'MSG_READ', payload: { from, userid, num } };
}

export function readMsg(from) {
    return (dispatch, getState) => {
        axios.post('/user/readmsg', { from })
            .then(res => {
                if (res.status === 200 && res.data.code === 0) {
                    const userid = getState().user._id;
                    dispatch(msgRead({ from, userid, num: res.data.num }));
                } else {
                    Toast.fail(res.data.msg);
                }
            })
    }
}