import { combineReducers } from 'redux';
import { type } from './action';
import { getRedirectPath } from '../utils';

function user(state = { user: '', identity: '' }, action) {
    switch (action.type) {
        case type.AUTH:
            return {
                ...state,
                ...action.payload,
                redirectTo: getRedirectPath(action.payload),
            }
        case type.LOGOUT:
            return {
                user: '',
                identity: '',
                redirectTo: '/login',
            }
        default:
            return state;
    }
}

function chat(state = { msgList: [], unread: 0 }, action) {
    switch (action.type) {
        case type.MSG_LIST:
            {
                const { data = [], users, userid } = action.payload;
                return {
                    ...state,
                    msgList: data,
                    users,
                    unread: data.filter(v => (!v.read) && (v.to === userid)).length
                }
            }
        case type.MSG_RECV:
            {
                const { data, userid } = action.payload;
                const n = (data.to === userid) ? 1 : 0;
                return {
                    ...state,
                    msgList: [...state.msgList, data],
                    unread: state.unread + n
                };
            }
        case type.MSG_READ:
            {
                const { from, num } = action.payload;
                const msgList = state.msgList.map(v => ({ ...v, read: from === v.from ? true : v.read }));
                return {
                    ...state,
                    msgList,
                    unread: state.unread - num
                }
            }
        default:
            return state;
    }
}

export default combineReducers({ user, chat });