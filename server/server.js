const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const userRouter = require('./user');
const model = require('./model');
const path = require('path');
const Chat = model.getModel('chat');
const app = express();

// Chat.remove({},()=>{});
// 配合Express使用Socket.io
const server = require('http').Server(app);
const io = require('socket.io')(server);

// 实时监听connection事件
io.on('connection', function (socket) {
    // 监听前端发送过来的sendmsg消息
    socket.on('sendmsg', function (data) {
        const { from, to, content } = data;
        const chatid = [from, to].sort().join('_');
        Chat.create({ chatid, from, to, content }, function (err, doc) {
            // 全局广播recvmsg消息
            io.emit('recvmsg', doc);
        })
    })
})

app.use(bodyParser.json());
app.use(cookieParser());
app.use('/user', userRouter);
app.use(function (req, res, next) {
    // 如果以'/user/' 或 '/static/' 开始，就认为不是我们处理的范围
    if (req.url.startsWith('/user/') || req.url.startsWith('/static/')) {
        return next();
    }
    // path.resolve:把相对路径变成绝对路径，确保不会报错
    return res.sendFile(path.resolve('build/index.html'));
});
app.use('/', express.static(path.resolve('build')));

server.listen(9093, function () {
    console.log('Node app start at port 9093');
});