const express = require('express');
const utility = require('utility');
const Router = express.Router();
const model = require('./model');
const User = model.getModel('user');
const Chat = model.getModel('chat');
const _filter = { 'pwd': 0, '__v': 0 };

// 获取用户列表
Router.get('/list', function (req, res) {
    const { identity } = req.query;
    User.find({ identity }, function (err, doc) {
        if (err) {
            return res.json({ code: 1, msg: '后端出错了' });
        }
        return res.json({ code: 0, data: doc });
    });
})

// 获取聊天列表
Router.get('/getMsgList', function (req, res) {
    const user = req.cookies.userid;
    User.find({}, function (e, userdoc) {
        let users = {};
        userdoc.forEach(v => {
            users[v._id] = { name: v.user, avatar: v.avatar };
        })
        Chat.find({ '$or': [{ from: user }, { to: user }] }, function (err, doc) {
            if (!err) {
                return res.json({ code: 0, data: doc, users });
            }
        })
    })
})

// 更新已读消息
Router.post('/readmsg', function (req, res) {
    const userid = req.cookies.userid;
    const { from } = req.body;
    Chat.update(
        { from, to: userid },
        { '$set': { read: true } },
        { 'multi': true },
        function (err, doc) {
            if (!err) {
                return res.json({ code: 0, num: doc.nModified });
            }
            return res.json({ code: 1, msg: '修改失败' });
        })
})

// 用户注册
Router.post('/register', function (req, res) {
    const { user, pwd, identity } = req.body;
    User.findOne({ user }, function (err, doc) {
        if (doc) {
            return res.json({ code: 1, msg: '用户名重复' });
        }
        User.create({ user, identity, pwd: md5Pws(pwd) }, function (err, doc) {
            if (err) {
                return res.json({ code: 1, msg: '后端出错了' });
            }
            // 设置cookie
            res.cookie('userid', doc._id);
            return res.json({ code: 0, data: doc });
        })
    })
})

// 用户登录
Router.post('/login', function (req, res) {
    const { user, pwd } = req.body;
    User.findOne({ user, pwd: md5Pws(pwd) }, _filter, function (err, doc) {
        if (!doc) {
            return res.json({ code: 1, msg: '用户名或密码错误' });
        }
        // 设置cookie
        res.cookie('userid', doc._id);
        return res.json({ code: 0, data: doc });
    })
})

// 用户更新
Router.post('/update', function (req, res) {
    const { userid } = req.cookies;
    if (!userid) {
        return res.json({ code: 1 });
    }
    User.findByIdAndUpdate(userid, req.body, function (err, doc) {
        const { user, identity } = doc;
        const data = { user, identity, ...req.body };
        return res.json({ code: 0, data });
    });
})

// 获取用户登录信息
Router.get('/info', function (req, res) {
    const { userid } = req.cookies;
    // 用户没有cookie
    if (!userid) {
        return res.json({ code: 1 });
    }
    // 用户有cookie
    User.findOne({ _id: userid }, _filter, function (err, doc) {
        if (err) {
            return res.json({ code: 1, msg: '后端出错了' });
        }
        if (doc) {
            return res.json({ code: 0, data: doc });
        }
    })
})

function md5Pws(pwd) {
    const salt = 'nian_is_beauty_7659@()*&~~';
    return utility.md5(utility.md5(pwd + salt));
}

module.exports = Router;