const mongoose = require('mongoose');

// 连接mongo，并且使用chat这个集合
const DB_URL = 'mongodb://127.0.0.1:27017/chat';
mongoose.connect(DB_URL);

// 定义文档模型
const models = {
    user: {
        'user': { type: String, require: true },
        'pwd': { type: String, require: true },
        'identity': { type: String, require: true },
        // 头像
        'avatar': { type: String },
        // 个人简介或职位简介
        'title': { type: String },
        'desc': { type: String },
        // 如果你是boss，还有两个字段
        'company': { type: String },
        'salary': { type: String },
    },
    chat: {
        'chatid': { type: String, require: true },
        'from': { type: String, require: true },
        'to': { type: String, require: true },
        'read': { type: Boolean, default: false },
        'content': { type: String, require: true, default: '' },
        'create_time': { type: Number, default: new Date().getTime() },
    }
}

// 批量生成文档
for (let m in models) {
    mongoose.model(m, new mongoose.Schema(models[m]));
}

module.exports = {
    // 读取文档
    getModel: function (name) {
        return mongoose.model(name);
    }
}